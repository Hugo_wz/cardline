class Animal:
    def __init__(self, nom: str, nomScientifique: str, taille: float, poids: float, longevite: int):
        self.__nom = nom
        self.__nomScientifique = nomScientifique
        self.__taille = taille
        self.__poids = poids
        self.__longevite = longevite

    def getnom(self):
        return self.__nom
    def getnomSci(self):
        return self.__nomScientifique
    def gettaille(self):
        return self.__taille
    def getpoids(self):
        return self.__poids
    def getlongevite(self):
        return self.__longevite

